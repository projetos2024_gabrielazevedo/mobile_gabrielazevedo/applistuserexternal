import React from "react";
import { View, Text, TouchacleOpacity, FlatList, StyleSheet } from "react-native";

const userDetails = ({ route })=>{
    const {user} = route.params;    
    
    return(
        <View>
            <Text style={styles.text}>Informações:</Text>
            <Text style={styles.texti}>Nome: {user.name}</Text>
            <Text style={styles.texti}>Email: {user.email}</Text>
            <Text style={styles.texti}>Rua: {user.address.street}</Text>
            <Text style={styles.texti}>Número da Casa: {user.address.suite}</Text>
            <Text style={styles.texti}>Cidade: {user.address.city}</Text>
            <Text style={styles.texti}>Telefone: {user.phone}</Text>
            <Text style={styles.texti}>Site: {user.website}</Text>
            <Text style={styles.texti}>Nome da compania: {user.company.name}</Text>
            <Text style={styles.texti}>Compania: {user.company.catchPhrase}</Text>
            <Text style={styles.texti}>Compania: {user.company.bs}</Text>

        </View>
    );
    
}
export default userDetails;

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        fontWeight:'bold',
        color:'red'
        
    },

    texti: {
        fontSize: 20,
        fontWeight:'bold',
       
    },
})