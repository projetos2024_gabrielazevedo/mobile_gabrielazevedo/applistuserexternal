import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";
import axios from "axios";

const List = ({navigation})=>{

const [users, setUsers] = useState([]);
useEffect(()=>{
    getUsers();
},[])

async function getUsers(){
    try{
        const response = await axios.get("https://jsonplaceholder.typicode.com/users");
        setUsers(response.data);
    } catch (error) {}

}


  const userPress = (user) => {
    navigation.navigate("DetalhesDosUsuarios", { user });
  };
  return (
    <View>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => userPress(item)}>
            <Text style={styles.texta}>{item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}
export default List;

const styles = StyleSheet.create({
    texta: {
        fontSize: 25,
        fontWeight:'bold',
        color:'black'
        
    },
})

