import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import List from "./src/list";
import userDetails from "./src/userDetails";



const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Usuários">
        <Stack.Screen name="Usuários" component={List} />
        <Stack.Screen
          name="DetalhesDosUsuarios"
          component={userDetails}
          options={{ title: "Informações dos Usuários" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
    
  );
}

